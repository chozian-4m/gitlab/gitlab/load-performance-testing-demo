# Load Performance Testing Demo

This is a demo of the GitLab [Load Performance Testing](https://docs.gitlab.com/ee/user/project/merge_requests/load_performance_testing.html) feature.

NOTE:
If the Load Performance report has no data to compare, such as when you add the
Load Performance job in your .gitlab-ci.yml for the very first time,
the Load Performance report widget doesn't display. It must have run at least
once on the target branch (main, for example), before it displays in a
merge request targeting that branch.

## Repository Structure

Load Testing requires a server to be deployed to an environment in order to work. This repository uses [Vercel](https://vercel.com) to host our environments.

### Vercel

Vercel provides a free account for proof of concepts. If you want to use this demo project for your own testing, you will need to create an account. You'll also need to set up two CI variables:

- VERCEL_SCOPE: this will be your vercel URL namespace. You can find this in your Vercel account settings.
- VERCEL_TOKEN: This is an access token you can generate in your Vercel account settings.
